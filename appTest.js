const request = require('supertest');
const expect = require('expect');
const rewire = require('rewire');

//var app = require('./app').app;

describe('Starting the Test Suit...',()=>{

    var stubbedApp = rewire('./app');

    //Creating A Mock Redis Client
    var client = {
        on: expect.createSpy(),
        hgetall: expect.createSpy(),
        hmset: expect.createSpy(),
        del: expect.createSpy()
    };
    //Creating a stubbed app injecting the mock
    stubbedApp.__set__('client',client);

    describe('Starting API TESTING>>>',()=>{
        it('Testing GET / (Positive)',(done)=>{
            request(stubbedApp.app)
                .get('/')
                .expect(200)
                .end(done)
        });
    
        it('Testing GET / (Negative)',(done)=>{
            request(stubbedApp.app)
                .get('/hhh')
                .expect(404)
                .end(done)
        });
    
        it('Testing POST /user/search ',(done)=>{
    
            request(stubbedApp.app)
                .post('/user/search')
                .expect(200)
                .end(done)
    
                done();
        });
    
        it('Testing Post /user/add',(done)=>{
            request(stubbedApp.app)
            .post('/user/add')
            .expect(200)
            .end(done)
    
            done();
        });
    
        it('Testing Delete /user/delete:id',(done)=>{
            request(stubbedApp.app)
            .post('/user/delete:id')
            .expect(404)
            .end(done)
        });
    });

    describe('Starting Redis Client Testing>>>',()=>{

        it('Testing Redis Client Initialization',()=>{
            client.on();
            expect(client.on).toHaveBeenCalled();
        })

        it('Testing Redis Client Search',(done)=>{
            var id ='user123';
            client.hgetall(id,(err,obj)=>{
                expect(obj).toHaveBeenCalled();
            });
    
            done();
        });

        it('Testing Redis Client Add',(done)=>{
            let id = 'user222';
            let first_name = 'firstName';
            let last_name = 'lastName';
            let email = 'xyz@ab.com';
            let phone = '1234567890';

            client.hmset(id,[
                'first_name', first_name,
                'last_name', last_name,
                'email', email,
                'phone', phone
            ],(err,reply)=>{
                expect(reply).toHaveBeenCalled();
            });

            done();
        });

        it('Testing Redis Client Delete',()=>{            
            var id = 'xyz123';
            client.del(id);
            expect(client.del).toHaveBeenCalledWith(id);

        });
    });

});